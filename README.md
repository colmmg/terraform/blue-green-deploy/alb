# blue-green-deploy/alb
The blue-green-deploy application load balancer Terraform code.

# Usage
```
terraform init -backend-config="region=$AWS_REGION"
terraform plan -var-file=tfvars/$AWS_ENVIRONMENT/$AWS_REGION/terraform.tfvars
terraform apply -var-file=tfvars/$AWS_ENVIRONMENT/$AWS_REGION/terraform.tfvars
```

# License and Authors
Authors: [Colm McGuigan](https://gitlab.com/colmmg)
