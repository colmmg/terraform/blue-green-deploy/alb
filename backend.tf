terraform {
  backend "s3" {
    bucket = "example-com-terraform-state"
    key    = "blue-green-deploy/alb/terraform.tfstate"
  }
}
