provider "aws" {
  region = "${var.region}"
}

data "aws_caller_identity" "current" {}

resource "aws_lb" "blue-green-deploy" {
  internal           = false
  load_balancer_type = "application"
  name               = "blue-green-deploy"
  security_groups    = ["${var.security_groups}"]
  subnets            = ["${var.subnets}"]
  tags = {
    Name = "blue-green-deploy"
  }
}

resource "aws_lb_listener" "blue-green-deploy" {
  load_balancer_arn = "${aws_lb.blue-green-deploy.arn}"
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.blue-green-deploy.arn}"
  }
}

resource "aws_lb_target_group" "blue-green-deploy" {
  name     = "blue-green-deploy"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}
