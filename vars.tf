variable "region" {
  default     = "us-east-1"
  description = "The AWS region."
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the LB."
  type        = "list"
}

variable "subnets" {
  description = "A list of subnet IDs to attach to the LB."
  type        = "list"
}

variable "vpc_id" {
  description = "The identifier of the VPC in which to create the target group"
}
